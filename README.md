# README #

This is the repository for testing the base pomp/mif2 code.  The model is a 3 strain status-based model that can be set to only 1 or 2 strains.  


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Install pomp R package first (http://kingaa.github.io/sbied/prep/preparation.html which worked for me or http://kingaa.github.io/pomp/install.html) 
2. Open the R script and set the name of the pdf file you'd like the plots to output to (line starting with the "pdf" function).
3. You can then run the R script in R on your PC, but it will take a few minutes even on low resolution (mostly due to numeric endemic equilibrium calculations) 
4. To submit the R script to Midway, I recommend using the R_job.sbatch script.  To use it, you'll have to specify the name of your R script to run, the job name, the output file and the error file.
5. Evaluate output from the pdf, which will contain multiple plots.

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact